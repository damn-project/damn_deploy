WITH lasts AS (
        SELECT DISTINCT ON (sid, aid) *
        FROM current_commits
        WHERE sid > 0
        ORDER BY aid, sid, cid DESC
),
locked AS (
        SELECT *
        FROM lasts
        WHERE type='locked'
),
expired AS (
        SELECT *
        FROM locked
        WHERE date + interval '2 hours' < now()
),
lasts2 AS (
	SELECT
            cc.sid,
            cc.aid,
            e.author,
            cc.type,
            row_number() OVER (PARTITION BY cc.sid ORDER BY cc.cid DESC) as rn
	FROM
                expired as e,
		current_commits as cc
	WHERE
                e.aid = cc.aid
                AND e.sid = cc.sid
)
INSERT INTO current_commits (sid, aid, author, type, message)
SELECT sid, aid, author, type, 'Unlock automatically due to timeout'
FROM lasts2 WHERE rn=2;
