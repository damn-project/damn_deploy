WITH areas AS (
    SELECT DISTINCT ON (sid, aid) aid, sid, type
    FROM current_commits
    WHERE (
        type='to map'
        OR type='to review'
        OR type='done'
        OR type='splitted'
    )
    ORDER BY aid, sid, cid DESC
),
to_map AS (
    SELECT aid, count(sid) as cnt
    FROM areas
    WHERE type='to map'
    GROUP BY aid
),
to_review AS (
    SELECT aid, count(sid) as cnt
    FROM areas
    WHERE type='to review'
    GROUP BY aid
),
done AS (
    SELECT aid, count(sid) as cnt
    FROM areas
    WHERE type='done'
    GROUP BY aid
)
UPDATE
    current_areas AS ca
SET
    squares_to_map=coalesce((SELECT cnt FROM to_map WHERE to_map.aid=ca.aid),0),
    squares_to_review=coalesce((SELECT cnt FROM to_review WHERE to_review.aid=ca.aid),0),
    squares_done=coalesce((SELECT cnt FROM done WHERE done.aid=ca.aid),0)
;
