#!/bin/bash
set -e

psql -d ${POSTGRES_DB} -U ${POSTGRES_USER} -c "CREATE EXTENSION postgis;"
