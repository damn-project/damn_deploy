alter table current_areas
add column squares_to_map bigint default 0,
add column squares_to_review bigint default 0,
add column squares_done bigint default 0;
