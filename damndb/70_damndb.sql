--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Debian 11.5-1+deb10u1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-1+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: st_area_split(public.geometry); Type: FUNCTION; Schema: public; Owner: damnuser
--

CREATE FUNCTION public.st_area_split(ar public.geometry, OUT public.geometry) RETURNS SETOF public.geometry
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
DECLARE
    jmax INTEGER := 32;
    imax INTEGER := 32;
    ssw DOUBLE PRECISION := (ST_XMax(ar) - ST_XMin(ar)) / jmax;
    ssh DOUBLE PRECISION := (ST_YMax(ar) - ST_YMin(ar)) / imax;
    srid INTEGER := 4326;
    ss_def geometry;
BEGIN
    CASE ST_SRID(ar) WHEN 0 THEN
        ar := ST_SetSRID(ar, srid);
        RAISE NOTICE'SRID Not Found.';
    ELSE
        RAISE NOTICE'SRID Found.';
    END CASE;

    IF ssw < 0.004 THEN
        ssw := 0.004;
        jmax := floor((ST_XMax(ar) - ST_XMin(ar)) / ssw) + 1;
    END IF;
    IF ssh < 0.004 THEN
        ssh := 0.004;
        imax := floor((ST_YMax(ar) - ST_YMin(ar)) / ssh) + 1;
    END IF;
    ss_def := ST_GeomFromText(
        FORMAT(
            'POLYGON((0 0, 0 %s, %s %s, %s 0, 0 0))',
            ssh, ssw, ssh, ssw
        ),
        srid
    );
    RETURN QUERY WITH foo AS (
        SELECT
            ST_Translate(
                ss_def,
                ST_XMin(ar) + j * ssw,
                ST_YMin(ar) + i * ssh
            ) AS ss
        FROM
            generate_series(0, imax) AS i,
            generate_series(0, jmax) AS j
    )
    SELECT
        ST_Intersection(ss, ar)
    FROM
        foo
    WHERE
        ST_intersects(ss, ar)
    ;
END;
$$;


ALTER FUNCTION public.st_area_split(ar public.geometry, OUT public.geometry) OWNER TO damnuser;

--
-- Name: st_square_split(public.geometry); Type: FUNCTION; Schema: public; Owner: damnuser
--

CREATE FUNCTION public.st_square_split(sq public.geometry, OUT public.geometry) RETURNS SETOF public.geometry
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
DECLARE
    ssw DOUBLE PRECISION := (ST_XMax(sq) - ST_XMin(sq)) / 2;
    ssh DOUBLE PRECISION := (ST_YMax(sq) - ST_YMin(sq)) / 2;
    srid INTEGER := 4326;
    ss_def geometry := ST_GeomFromText(
        FORMAT(
            'POLYGON((0 0, 0 %s, %s %s, %s 0, 0 0))',
            ssh, ssw, ssh, ssw
        ),
        srid
    );
BEGIN
    CASE ST_SRID(sq) WHEN 0 THEN
    sq := ST_SetSRID(sq, srid);
        RAISE NOTICE'SRID Not Found.';
    ELSE
        RAISE NOTICE'SRID Found.';
    END CASE;

    RETURN QUERY WITH foo AS (
        SELECT
            ST_Translate(
                ss_def,
                ST_XMin(sq) + j * ssw,
                ST_YMin(sq) + i * ssh
            ) AS ss
        FROM
            generate_series(0, 1) AS i,
            generate_series(0, 1) AS j
    )
    SELECT
        ST_Intersection(ss, sq)
    FROM
        foo
    WHERE
        ST_intersects(ss, sq)
    ;
END;
$$;


ALTER FUNCTION public.st_square_split(sq public.geometry, OUT public.geometry) OWNER TO damnuser;

--
-- Name: current_areas_aid_seq; Type: SEQUENCE; Schema: public; Owner: damnuser
--

CREATE SEQUENCE public.current_areas_aid_seq
    START WITH 1000
    INCREMENT BY 1
    MINVALUE 1000
    MAXVALUE 9999
    CACHE 1
    CYCLE;


ALTER TABLE public.current_areas_aid_seq OWNER TO damnuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: current_areas; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.current_areas (
    aid smallint DEFAULT nextval('public.current_areas_aid_seq'::regclass) NOT NULL,
    tags text,
    priority smallint,
    description json,
    instructions json,
    featurecollection json,
    created timestamp without time zone DEFAULT (now() at time zone 'utc')
);


ALTER TABLE public.current_areas OWNER TO damnuser;

--
-- Name: current_commits; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.current_commits (
    cid bigint NOT NULL,
    sid bigint,
    aid smallint,
    date timestamp without time zone DEFAULT (now() at time zone 'utc'),
    author text,
    type text,
    message text
);


ALTER TABLE public.current_commits OWNER TO damnuser;

--
-- Name: current_commits_cid_seq; Type: SEQUENCE; Schema: public; Owner: damnuser
--

CREATE SEQUENCE public.current_commits_cid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.current_commits_cid_seq OWNER TO damnuser;

--
-- Name: current_commits_cid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: damnuser
--

ALTER SEQUENCE public.current_commits_cid_seq OWNED BY public.current_commits.cid;


--
-- Name: current_squares; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.current_squares (
    sid bigint NOT NULL,
    aid smallint NOT NULL,
    border public.geometry
);


ALTER TABLE public.current_squares OWNER TO damnuser;

--
-- Name: finished_areas; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.finished_areas (
    id bigint NOT NULL,
    aid smallint,
    tags text,
    priority smallint,
    description json,
    instructions json,
    featurecollection json,
    created timestamp without time zone,
    finished timestamp without time zone DEFAULT (now() at time zone 'utc')
);


ALTER TABLE public.finished_areas OWNER TO damnuser;

--
-- Name: finished_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: damnuser
--

CREATE SEQUENCE public.finished_areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.finished_areas_id_seq OWNER TO damnuser;

--
-- Name: finished_areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: damnuser
--

ALTER SEQUENCE public.finished_areas_id_seq OWNED BY public.finished_areas.id;


--
-- Name: finished_commits; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.finished_commits (
    cid bigint NOT NULL,
    sid bigint,
    aid bigint,
    date timestamp without time zone,
    author text,
    type text,
    message text
);


ALTER TABLE public.finished_commits OWNER TO damnuser;

--
-- Name: finished_commits_cid_seq; Type: SEQUENCE; Schema: public; Owner: damnuser
--

CREATE SEQUENCE public.finished_commits_cid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.finished_commits_cid_seq OWNER TO damnuser;

--
-- Name: finished_commits_cid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: damnuser
--

ALTER SEQUENCE public.finished_commits_cid_seq OWNED BY public.finished_commits.cid;


--
-- Name: finished_squares; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.finished_squares (
    id bigint NOT NULL,
    sid bigint,
    aid bigint,
    border polygon
);


ALTER TABLE public.finished_squares OWNER TO damnuser;

--
-- Name: finished_squares_id_seq; Type: SEQUENCE; Schema: public; Owner: damnuser
--

CREATE SEQUENCE public.finished_squares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.finished_squares_id_seq OWNER TO damnuser;

--
-- Name: finished_squares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: damnuser
--

ALTER SEQUENCE public.finished_squares_id_seq OWNED BY public.finished_squares.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: damnuser
--

CREATE TABLE public.users (
    display_name text NOT NULL,
    info json
);


ALTER TABLE public.users OWNER TO damnuser;

--
-- Name: current_commits cid; Type: DEFAULT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_commits ALTER COLUMN cid SET DEFAULT nextval('public.current_commits_cid_seq'::regclass);


--
-- Name: finished_areas id; Type: DEFAULT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_areas ALTER COLUMN id SET DEFAULT nextval('public.finished_areas_id_seq'::regclass);


--
-- Name: finished_commits cid; Type: DEFAULT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_commits ALTER COLUMN cid SET DEFAULT nextval('public.finished_commits_cid_seq'::regclass);


--
-- Name: finished_squares id; Type: DEFAULT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_squares ALTER COLUMN id SET DEFAULT nextval('public.finished_squares_id_seq'::regclass);


--
-- Data for Name: current_areas; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.current_areas (aid, tags, priority, description, instructions, featurecollection, created) FROM stdin;
\.


--
-- Data for Name: current_commits; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.current_commits (cid, sid, aid, date, author, type, message) FROM stdin;
\.


--
-- Data for Name: current_squares; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.current_squares (sid, aid, border) FROM stdin;
\.


--
-- Data for Name: finished_areas; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.finished_areas (id, aid, tags, priority, description, instructions, featurecollection, created, finished) FROM stdin;
\.


--
-- Data for Name: finished_commits; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.finished_commits (cid, sid, aid, date, author, type, message) FROM stdin;
\.


--
-- Data for Name: finished_squares; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.finished_squares (id, sid, aid, border) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: damnuser
--

COPY public.users (display_name, info) FROM stdin;
\.


--
-- Name: current_areas_aid_seq; Type: SEQUENCE SET; Schema: public; Owner: damnuser
--

SELECT pg_catalog.setval('public.current_areas_aid_seq', 6864, true);


--
-- Name: current_commits_cid_seq; Type: SEQUENCE SET; Schema: public; Owner: damnuser
--

SELECT pg_catalog.setval('public.current_commits_cid_seq', 906889, true);


--
-- Name: finished_areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: damnuser
--

SELECT pg_catalog.setval('public.finished_areas_id_seq', 1, false);


--
-- Name: finished_commits_cid_seq; Type: SEQUENCE SET; Schema: public; Owner: damnuser
--

SELECT pg_catalog.setval('public.finished_commits_cid_seq', 1, false);


--
-- Name: finished_squares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: damnuser
--

SELECT pg_catalog.setval('public.finished_squares_id_seq', 1, false);


--
-- Name: current_areas current_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_areas
    ADD CONSTRAINT current_areas_pkey PRIMARY KEY (aid);


--
-- Name: current_commits current_commits_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_commits
    ADD CONSTRAINT current_commits_pkey PRIMARY KEY (cid);


--
-- Name: current_squares current_squares_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_squares
    ADD CONSTRAINT current_squares_pkey PRIMARY KEY (sid, aid);


--
-- Name: finished_areas finished_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_areas
    ADD CONSTRAINT finished_areas_pkey PRIMARY KEY (id);


--
-- Name: finished_commits finished_commits_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_commits
    ADD CONSTRAINT finished_commits_pkey PRIMARY KEY (cid);


--
-- Name: finished_squares finished_squares_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_squares
    ADD CONSTRAINT finished_squares_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (display_name);


--
-- Name: current_commits current_commits_aid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_commits
    ADD CONSTRAINT current_commits_aid_fkey FOREIGN KEY (aid) REFERENCES public.current_areas(aid);


--
-- Name: current_commits current_commits_author_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_commits
    ADD CONSTRAINT current_commits_author_fkey FOREIGN KEY (author) REFERENCES public.users(display_name);


--
-- Name: current_commits current_commits_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_commits
    ADD CONSTRAINT current_commits_sid_fkey FOREIGN KEY (sid, aid) REFERENCES public.current_squares(sid, aid);


--
-- Name: current_squares current_squares_aid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.current_squares
    ADD CONSTRAINT current_squares_aid_fkey FOREIGN KEY (aid) REFERENCES public.current_areas(aid);


--
-- Name: finished_commits finished_commits_aid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_commits
    ADD CONSTRAINT finished_commits_aid_fkey FOREIGN KEY (aid) REFERENCES public.finished_areas(id);


--
-- Name: finished_commits finished_commits_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_commits
    ADD CONSTRAINT finished_commits_sid_fkey FOREIGN KEY (sid) REFERENCES public.finished_squares(id);


--
-- Name: finished_squares finished_squares_aid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: damnuser
--

ALTER TABLE ONLY public.finished_squares
    ADD CONSTRAINT finished_squares_aid_fkey FOREIGN KEY (aid) REFERENCES public.finished_areas(id);


--
-- PostgreSQL database dump complete
--

