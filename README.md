Damn deploy
===========

`damn_deploy` is part of **Divide and map. Now.** -- the damn project.

The *damn project* helps mappers by dividing some big area into smaller squares
that a human can map.

The *damn deploy* is complete deployment guide.

License
-------

This project is published under [MIT License][].

[MIT License]: ./LICENSE

How to deploy
=============

Prerequisities for deployment are `git`, `docker` and `docker-compose`. The
guide is tested on Debian 10. You need some domain name directing to the
server.

Then, you need to clone the damn deploy repository:

```
git clone https://gitlab.com/damn-project/damn_deploy.git
cd damn_deploy
```

The most common usecase is *autostart server and clients*. The following
sections consider *server* setup, setup for *clients* (client and manager),
*chat*, *autostart*, and *update*. Also, see *Damn upkeep* section for periodic
database upkeep scripts.

Server setup
------------

1. Set environment variables in `env` file. The following is the meaning:

    - `DAMN_SERVER`: Use your domain.
    - `DAMN_SERVER_VERSION`: Choose the version of the server. Default is `master`.
    - `DAMN_CLIENTS`: Allow origins clients, default is all clients (`*`).
    - `DB_HOST`: Alias of `db` docker service, keep that value.

    - `POSTGRES_PASSWORD`: Password to PostgreSQL database.
    - `JWT_SECRET`: Secret for generating, signing, and decrypting JSON Web Tokens.
    - `SESSION_SECRET`: Secret for cookies (I think).
    - `OAUTH_CONSUMER_KEY`: API key, get from osm.org.
    - `OAUTH_CONSUMER_SECRET`: API secret, get from osm.org.

2. Set proper email address in `traefik.yml` file.

3. Create `acme.json` file and set `600` permissions:

   ```
   touch acme.json
   chmod 600 acme.json
   ```

4. You may run the damn server now.

   ```
   docker-compose up
   ```

Setup for clients
-----------------

In addition to *Server setup*, you may want to run a client and a manager, too.

1. Set environment variables in `env` file. The following is the meaning:

    - `DAMN_CLIENT`: DNS of client.
    - `DAMN_CLIENT_REPO`: Link to client git repository.
    - `DAMN_CLIENT_VERSION`: Version of the client.
    - `DAMN_MANAGER`: DNS of manager.
    - `DAMN_MANAGER_REPO`: Link to the manager git repository.
    - `DAMN_MANAGER_VERSION`: Version of the manager.
    - `DEFAULT_LANG`: The default language.
    - `DEFAULT_AREAS_VIEW`: Show areas on first page by default as `grid` or
      `list`?
    - `DEFAULT_WEB_EDITOR`: Set the default editor to `id` or `rapid-esri`.

2. You may run the damn server with clients now.

   ```
   docker-compose -f docker-compose.yml -f docker-compose-clients.yml up
   ```

Setup for chat
--------------

In addition to *Server setup*, you may want to run a chat server.

1. Set environment variable in `env` file.

    - `DAMN_CHAT`: DNS of chat server.

2. Run the chat server with the server:

   ```
   docker-compose -f docker-compose.yml -f docker-compose-chat.yml up
   ```

   or also with clients:

   ```
   docker-compose -f docker-compose.yml -f docker-compose-clients.yml -f docker-compose-chat.yml up
   ```

Autostart with `systemd`
------------------------

I assume that the damn deploy repository is cloned to `/root/damn_deploy/`.

1. Create systemd unit for `docker-compose`:

   ```
   cat << EOF >> /etc/systemd/system/damn.service
   [Unit]
   Description=Damn server app
   After=network.target docker.service

   [Service]
   Type=simple
   WorkingDirectory=/root/damn_deploy
   ExecStart=/usr/bin/docker-compose -f /root/damn_deploy/docker-compose.yml -f /root/damn_deploy/docker-compose-clients.yml up
   ExecStop=/usr/bin/docker-compose -f /root/damn_deploy/docker-compose.yml -f /root/damn_deploy/docker-compose-clients.yml down

   [Install]
   WantedBy=multi-user.target
   EOF
   ```

   Remove ` -f /root/damn_deploy/docker-compose-clients.yml` when running
   server only.

2. Enable and run damn service:

   ```
   systemctl start damn.service
   systemctl enable damn.service
   ```


How to update
=============

1. If use `systemd` autostart, stop the damn service first:

   ```
   systemctl stop damn.service
   ```

2. Stash the configuration:

   ```
   git stash
   ```

3. Pull new version of the damn deploy repository:

   ```
   git pull
   ```

4. Pop stashed changes (renew the configuration):

   ```
   git stash pop
   ```

5. Maybe rebuild some docker images, but it depends on update changes:

   ```
   docker-compose build --no-cache api
   docker-compose build --no-cache upkeep
   ```

6. Start the damn service again:

   ```
   systemctl start damn.service
   ```

Update database structure
-------------------------

The database structure is rarely to change but it may happen. For new
deployments, there is nothing to do.

However, if you are upgrading from an existing database, you need to run the
upgrade script manually.

All the scripts related to the database structure are stored in `damndb`
directory, starting at `70_...`. Which script should be run when updating the
database structure is always noted in the [changelog][].

Upgrade to `v0.6.0`
-------------------

Stop the damn service and update the repository:

```
systemctl stop damn.service
git stash
git pull
git stash pop
```

Run the database:

```
docker-compose up -d db
```

Run the database upgrade script (you will be asked for the password stored in
`.env` file):

```
export DB_HOST=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' damndb)
psql -h $DB_HOST -d damndb -U damnuser < damndb/71_areas_squares_stats.sql
```

Stop the database container:

```
docker-compose down
```

Finally, there is no need to build the database container again. The database
is created only once anyway, so run the damn service again:

```
systemctl start damn.service
```

Upgrade to `v0.7.0`
-------------------

When the upgrade is finished, update the `current_commits` in the database.
Connect to the database (the password is in `.env` file):

```
export DB_HOST=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' damndb)
psql -h $DB_HOST -d damndb -U damnuser
```

Run update query:

```
update current_commits set type='splitted' where message='The square was splitted';
```


Damn upkeep
===========

Damn upkeep docker image contains scripts that run periodically. Periodic run
must be set with `cron` or `systemd`. The following example uses `systemd`.

An example of upkeep script is square unlock when there is no activity in two
hours.

1. Create systemd unit and timer for damn upkeep:

   ```
   cat << EOF >> /etc/systemd/system/damn_upkeep.service
   [Unit]
   Description=Run damn upkeep

   [Service]
   WorkingDirectory=/root/damn_deploy
   ExecStart=/usr/bin/docker-compose -f /root/damn_deploy/docker-compose.yml run --rm upkeep
   EOF
   cat << EOF >> /etc/systemd/system/damn_upkeep.timer
   [Unit]
   Description=Run damn upkeep every 15 minutes

   [Timer]
   OnCalendar=*:0/15

   [Install]
   WantedBy=timers.target
   EOF
   ```

2. Enable and run damn upkeep service and timer:

   ```
   systemctl enable damn_upkeep.timer
   systemctl enable damn_upkeep.service

   systemctl start damn_upkeep.timer
   ```

Contribute
==========

Use [OneFlow][] branching model and keep the [changelog][].

Write [great git commit messages][]:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[OneFlow]: https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[changelog]: ./CHANGELOG.md
[great git commit messages]: https://chris.beams.io/posts/git-commit/
