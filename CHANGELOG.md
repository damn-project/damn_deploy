Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

[Unreleased][]
==============


[0.7.0][] - 2020-10-17
======================

Upgrades
--------

- `damn_server` to [v0.6.1][s0.6.1].

  **NOTE:** You need to upgrade database content to make sure the statistics
  works. Please, see *Upgrade to `v0.7.0`* section of [readme][] file.
- `damn_client` to [v0.11.2][c0.11.2].
- `damn_jsonmanager` to [v0.1.5][m0.1.5].

Added
-----

- Database upgrade section to [readme][].
- Create static damn client script.

Changed
-------

- Markdown reformat.

[readme]: ./README.md
[s0.6.1]: https://gitlab.com/damn-project/damn_server/-/tags/v0.6.1
[c0.11.2]: https://gitlab.com/damn-project/damn_client/-/tags/v0.11.2
[m0.1.5]: https://gitlab.com/damn-project/damn_jsonmanager/-/tags/v0.1.5


[0.6.2][] - 2020-10-01
======================

Fixed
-----

- Update json manager version.


[0.6.1][] - 2020-10-01
======================

Fixed
-----
- Update server and client version.


[0.6.0][] - 2020-10-01
======================

Added
-----

- Some damn client constants. Get ready for the next damn client release.
- Statistics to `current_areas` table. Also, upkeep script to update these
  statistics.

  **NOTE:** You need to upgrade database structure. Please, see *Upgrade to
  `v0.6.0`* section of [readme][] file.

Changed
-------

- Swich client to `v0.10.0` that support multiple languages.


[0.5.0][] - 2020-08-18
======================

Added
-----

- Docker image, docker compose for [WSChat](https://sr.ht/~qeef/wschat/).


[0.4.2][] - 2020-06-16
======================

Fixed
-----

- Set maximum of 100 workers due to maximum of 100 database connections.


[0.4.1][] - 2020-06-02
======================

Fixed
-----

- Upkeep script.


[0.4.0][] - 2020-04-29
======================

Added
-----

- HTTP -> HTTPS redirect for clients.

Changed
-------

- Update deploy guide.
- Update server to `v0.5.0`.


[0.3.0][] - 2020-02-01
======================

Added
-----

- Second docker compose configuration for running clients (client and manager).


[0.2.2][] - 2020-01-28
======================

Changed
-------

- Server update to version 0.4.0.

[0.2.1][] - 2020-01-14
======================

Fixed
-----

- Use UTC now() for default timestamps in database.


[0.2.0][] - 2020-01-13
======================

Changed
-------

- Damn server version to v0.3.0.


[0.1.2][] - 2020-01-08
======================

Changed
-------

- Update create database sql script.


[0.1.1][] - 2020-01-01
======================

Fixed
-----

- Damn server version.


0.1.0 - 2020-01-01
==================

Added
-----

- Changelog, license, readme.
- Docker file for damn db image.
- Docker file for damn server image.
- Docker compose file.
- Docker file for damn upkeep image.
- Traefik docker image, restrict damn server to https.


[Unreleased]: https://gitlab.com/damn-project/damn_deploy/compare/v0.7.0...master
[0.7.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.6.2...v0.7.0
[0.6.2]: https://gitlab.com/damn-project/damn_deploy/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/damn-project/damn_deploy/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/damn-project/damn_deploy/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/damn-project/damn_deploy/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.2.2...v0.3.0
[0.2.2]: https://gitlab.com/damn-project/damn_deploy/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/damn-project/damn_deploy/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/damn-project/damn_deploy/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/damn-project/damn_deploy/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/damn-project/damn_deploy/compare/v0.1.0...v0.1.1
